resource "kubernetes_deployment" "hash_compare" {
  metadata {
    name = "hash-compare"
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = "hash-compare"
      }
    }

    template {
      metadata {
        labels = {
          app = "hash-compare"
        }
      }

      spec {
        container {
          name = "hash-compare"
          image = "farhanluckali/hash_python"

          port {
            container_port = 5000
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "hash_compare" {
  metadata {
    name = "hash-compare"
  }

  spec {
    selector = {
      app = "hash-compare"
    }

    port {
      protocol    = "TCP"
      port        = 5000
      target_port = 5000
    }

    type = "ClusterIP"
  }
}

