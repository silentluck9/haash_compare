from flask import Flask, request, jsonify, render_template
from hashlib import sha256

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/generate_hash', methods=['POST'])
def generate_hash():
    birthdate = request.form.get('birthdate')
    first_name = request.form.get('first_name')
    last_name = request.form.get('last_name')

    if not birthdate or not first_name or not last_name:
        return jsonify({'error': 'Missing fields'}), 400

    data_str = birthdate + first_name + last_name
    hash_object = sha256()
    hash_object.update(data_str.encode('utf-8'))
    hash_hex = hash_object.hexdigest()

    app.hash_object = hash_object

    return jsonify({'hash': hash_hex}), 200

@app.route('/compare_hash', methods=['POST'])
def compare_hash():
    user_hash = request.form.get('hash')

    if not user_hash:
        return jsonify({'error': 'Missing hash'}), 400

    if not hasattr(app, 'hash_object'):
        return jsonify({'error': 'Hash not generated'}), 500

    generated_hash_hex = app.hash_object.hexdigest()

    if user_hash == generated_hash_hex:
        match = True
    else:
        match = False

    return jsonify({'match': match}), 200

if __name__ == '__main__':
      app.run(host='0.0.0.0', port=5000)
